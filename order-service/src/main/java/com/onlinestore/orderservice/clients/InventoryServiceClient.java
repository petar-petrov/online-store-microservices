package com.onlinestore.orderservice.clients;

import com.onlinestore.orderservice.dtos.InventoryResponse;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "inventory-service")
@Component
public interface InventoryServiceClient {

    @GetMapping("/api/inventory/in-stock")
    List<InventoryResponse> areInStock(@RequestParam(name = "sku") List<String> skuList);

    @PutMapping("/api/inventory/quantity")
    void updateQuantity(@RequestParam String sku, @RequestParam int quantity);

}
