package com.onlinestore.orderservice.controllers;

import com.onlinestore.orderservice.dtos.OrderDto;
import com.onlinestore.orderservice.mappers.OrderMapper;
import com.onlinestore.orderservice.models.Order;
import com.onlinestore.orderservice.services.contracts.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    private final OrderMapper orderMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String placeOrder(@RequestBody OrderDto orderDto) {
        Order newOrder = orderMapper.dtoToOrder(orderDto);
        orderService.createOrder(newOrder);
        return "Order Placed Successfuly";
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto getOrder(@PathVariable long id) {
        Order order = orderService.getOrder(id);
        return orderMapper.orderToDto(order);
    }
}
