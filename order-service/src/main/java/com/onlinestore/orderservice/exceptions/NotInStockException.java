package com.onlinestore.orderservice.exceptions;

public class NotInStockException extends RuntimeException {
    public NotInStockException() {
    }

    public NotInStockException(String message) {
        super(message);
    }
}
