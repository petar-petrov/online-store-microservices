package com.onlinestore.orderservice.repositories;

import com.onlinestore.orderservice.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Long> {
}
