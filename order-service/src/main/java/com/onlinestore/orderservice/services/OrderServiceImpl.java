package com.onlinestore.orderservice.services;

import com.onlinestore.orderservice.clients.InventoryServiceClient;
import com.onlinestore.orderservice.dtos.InventoryResponse;
import com.onlinestore.orderservice.exceptions.NotInStockException;
import com.onlinestore.orderservice.models.Order;
import com.onlinestore.orderservice.models.OrderLineItem;
import com.onlinestore.orderservice.repositories.OrderRepository;
import com.onlinestore.orderservice.services.contracts.OrderService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final InventoryServiceClient inventoryServiceClient;

    @Override
    public void createOrder(Order order) {
        List<OrderLineItem> orderItems = order.getOrderLineItemList();

        confirmItemsInStock(orderItems);
        orderRepository.save(order);
        updateItemListQuantity(orderItems);
    }

    private void updateItemListQuantity(List<OrderLineItem> orderItems) {
        orderItems.forEach(lineItem -> {
            String sku = lineItem.getSku();
            //This needs to be negative to remove the quantity and not add it.
            int quantityToRemove = -lineItem.getQuantity();
            inventoryServiceClient.updateQuantity(sku,quantityToRemove);
        });
    }

    private void confirmItemsInStock(List<OrderLineItem> orderItems) {
        List<String> skuList = orderItems.stream()
                .map(OrderLineItem::getSku)
                .toList();

            List<InventoryResponse> inventoryList = inventoryServiceClient.areInStock(skuList);

        for (InventoryResponse inventoryResponse : inventoryList) {
            if(!inventoryResponse.isInStock()){
                throw new NotInStockException(String.format("Sku %s has insufficient quantity in stock",
                        inventoryResponse.sku()));
            }
        }
    }

    @Override
    public Order getOrder(long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);

        if(orderOptional.isEmpty()) {
            throw new EntityNotFoundException("bad Id");
        }

        return orderOptional.get();
    }
}
