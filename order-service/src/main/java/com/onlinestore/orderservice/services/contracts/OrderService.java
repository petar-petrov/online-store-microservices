package com.onlinestore.orderservice.services.contracts;

import com.onlinestore.orderservice.models.Order;

public interface OrderService {

    public void createOrder(Order order);

    public Order getOrder(long id);

}
