package com.onlinestore.orderservice.mappers;

import com.onlinestore.orderservice.dtos.OrderLineItemDto;
import com.onlinestore.orderservice.dtos.OrderDto;
import com.onlinestore.orderservice.models.Order;
import com.onlinestore.orderservice.models.OrderLineItem;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderMapper {

    public OrderLineItem dtoToLineItem(OrderLineItemDto lineItemDto) {
        return OrderLineItem.builder()
                .sku(lineItemDto.sku())
                .price(lineItemDto.price())
                .quantity(lineItemDto.quantity())
                .build();
    }

    public OrderLineItemDto lineItemToDto(OrderLineItem lineItem) {
        return OrderLineItemDto.builder()
                .sku(lineItem.getSku())
                .price(lineItem.getPrice())
                .quantity(lineItem.getQuantity())
                .build();
    }

    public Order dtoToOrder(OrderDto orderDto) {
        List<OrderLineItemDto> dtoList = orderDto.orderLineItemDtoList();
        List<OrderLineItem> mappedList = dtoList.stream()
                .map(this::dtoToLineItem)
                .toList();

        return Order.builder()
                .orderLineItemList(mappedList)
                .build();
    }

    public OrderDto orderToDto(Order order) {
        List<OrderLineItem> itemList = order.getOrderLineItemList();
        List<OrderLineItemDto> mappedList = itemList.stream()
                .map(this::lineItemToDto)
                .toList();

        return OrderDto.builder()
                .orderLineItemDtoList(mappedList)
                .build();
    }
}
