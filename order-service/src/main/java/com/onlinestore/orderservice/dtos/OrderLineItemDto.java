package com.onlinestore.orderservice.dtos;

import lombok.Builder;

import java.math.BigDecimal;


@Builder
public record OrderLineItemDto(String sku, BigDecimal price, int quantity) {
}
