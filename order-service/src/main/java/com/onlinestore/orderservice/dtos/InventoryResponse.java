package com.onlinestore.orderservice.dtos;

public record InventoryResponse(String sku, boolean isInStock) {
}
