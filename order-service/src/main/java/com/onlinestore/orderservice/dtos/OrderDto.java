package com.onlinestore.orderservice.dtos;

import lombok.Builder;

import java.util.List;

@Builder
public record OrderDto(List<OrderLineItemDto> orderLineItemDtoList) {

}
