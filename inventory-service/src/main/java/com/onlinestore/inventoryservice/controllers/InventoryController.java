package com.onlinestore.inventoryservice.controllers;

import com.onlinestore.inventoryservice.dtos.InventoryResponse;
import com.onlinestore.inventoryservice.mappers.InventoryMapper;
import com.onlinestore.inventoryservice.services.contratcts.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventory")
@RequiredArgsConstructor
public class InventoryController {

    private final InventoryService inventoryService;

    @GetMapping("/in-stock")
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryResponse> isInStock(@RequestParam(name="sku") List<String> skuList) {
        return inventoryService.isInStock(skuList);
    }

    @PutMapping("/quantity")
    @ResponseStatus(HttpStatus.OK)
    public void updateQuantity(@RequestParam String sku, @RequestParam int quantity) {
        inventoryService.updateQuantity(sku, quantity);
    }

}
