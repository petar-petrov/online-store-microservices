package com.onlinestore.inventoryservice.services.contratcts;

import com.onlinestore.inventoryservice.dtos.InventoryResponse;

import java.util.List;

public interface InventoryService {

    boolean isInStock(String sku);

    void updateQuantity(String sku, int isAddQuantity);

    List<InventoryResponse> isInStock(List<String> skuList);
}
