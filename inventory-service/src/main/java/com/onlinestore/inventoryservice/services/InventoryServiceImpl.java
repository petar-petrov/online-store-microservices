package com.onlinestore.inventoryservice.services;

import com.onlinestore.inventoryservice.dtos.InventoryResponse;
import com.onlinestore.inventoryservice.models.Inventory;
import com.onlinestore.inventoryservice.repositories.InventoryRepository;
import com.onlinestore.inventoryservice.services.contratcts.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InventoryServiceImpl implements InventoryService {

    private final InventoryRepository inventoryRepository;

    @Transactional(readOnly = true)
    @Override
    public boolean isInStock(String sku) {
        Optional<Inventory> inventoryOptional = inventoryRepository.findBySku(sku);
        return inventoryOptional.isPresent() && inventoryOptional.get().getQuantity() != 0;
    }

    @Override
    public void updateQuantity(String sku, int quantity) {
        if (quantity == 0) {
            throw new IllegalArgumentException("Quantity cannot be 0");
        }
        Optional<Inventory> inventoryOptional = inventoryRepository.findBySku(sku);
        Inventory inventory = inventoryOptional.orElseGet(() -> new Inventory(0L, sku, 0));

        updateQuantity(inventory, quantity);

    }

    @Transactional(readOnly = true)
    @Override
    public List<InventoryResponse> isInStock(List<String> skuList) {
        List<InventoryResponse> result = skuList.stream()
                .map(sku -> {
                    boolean isInStock = isInStock(sku);
                    return new InventoryResponse(sku, isInStock);
                }).toList();

        return result;
    }

    private void updateQuantity(Inventory inventory, int quantity) {
        throwIfInsufficientQuantity(inventory.getQuantity(), quantity);

        inventory.setQuantity(inventory.getQuantity() + quantity);
        inventoryRepository.save(inventory);
    }

    private void throwIfInsufficientQuantity(int existingQuantity, int quantityToBeRemoved) {
        boolean isNegativeQuantity = quantityToBeRemoved < 0;
        if (isNegativeQuantity && existingQuantity < Math.abs(quantityToBeRemoved)) {
            throw new IllegalArgumentException("Insufficient item quantity, cannot remove");
        }
    }
}
