package com.onlinestore.inventoryservice.dtos;

public record InventoryResponse(String sku, boolean isInStock) {

}
