package com.onlinestore.productservice.services;

import com.onlinestore.productservice.mappers.ProductMapper;
import com.onlinestore.productservice.models.Product;
import com.onlinestore.productservice.repositories.ProductRepository;
import com.onlinestore.productservice.services.contracts.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void createProduct(Product product) {
        Product savedProduct = productRepository.insert(product);
        log.info("Product {} was saved and assigned id {}",
                savedProduct.getName(), savedProduct.getId());
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
