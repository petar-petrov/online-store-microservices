package com.onlinestore.productservice.services.contracts;

import com.onlinestore.productservice.models.Product;

import java.util.List;

public interface ProductService {

    void createProduct(Product product);

    List<Product> getAllProducts();

}
