package com.onlinestore.productservice.repositories;

import com.onlinestore.productservice.models.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {
}
