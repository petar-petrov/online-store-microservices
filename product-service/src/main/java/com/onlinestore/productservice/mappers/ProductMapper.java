package com.onlinestore.productservice.mappers;

import com.onlinestore.productservice.dtos.ProductRequest;
import com.onlinestore.productservice.dtos.ProductResponse;
import com.onlinestore.productservice.dtos.ProductResponseList;
import com.onlinestore.productservice.models.Product;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {

    public Product requestToProduct(ProductRequest request) {
        String name = request.getName();
        String description = request.getDescription();
        BigDecimal price = request.getPrice();
        return Product.builder()
                .name(name)
                .description(description)
                .price(price)
                .build();
    }

    public ProductResponse productToResponse(Product product) {
        return ProductResponse.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .id(product.getId())
                .build();
    }


    public ProductResponseList productListToResponseList(List<Product> productList) {
        List<ProductResponse> responseList = productList.stream()
                .map(this::productToResponse)
                .toList();

        return ProductResponseList.builder()
                .responseList(responseList)
                .build();
    }
}
