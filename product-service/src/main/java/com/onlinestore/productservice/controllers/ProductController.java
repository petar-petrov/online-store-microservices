package com.onlinestore.productservice.controllers;

import com.onlinestore.productservice.dtos.ProductRequest;
import com.onlinestore.productservice.dtos.ProductResponseList;
import com.onlinestore.productservice.mappers.ProductMapper;
import com.onlinestore.productservice.models.Product;
import com.onlinestore.productservice.services.contracts.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    private final ProductMapper productMapper;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestBody ProductRequest request) {
        Product product = productMapper.requestToProduct(request);
        productService.createProduct(product);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ProductResponseList getAllProducts() {
        List<Product> productList = productService.getAllProducts();
        return productMapper.productListToResponseList(productList);
    }

}
