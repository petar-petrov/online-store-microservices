package com.onlinestore.productservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestore.productservice.dtos.ProductRequest;
import com.onlinestore.productservice.dtos.ProductResponse;
import com.onlinestore.productservice.dtos.ProductResponseList;
import com.onlinestore.productservice.mappers.ProductMapper;
import com.onlinestore.productservice.models.Product;
import com.onlinestore.productservice.repositories.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;
import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
class ProductServiceApplicationTests {
    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:6.0");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;


    @DynamicPropertySource
    static void configurePropertySource(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Test
    void createShouldCreateProduct() throws Exception {
        ProductRequest request = getProductRequest();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated());
        Assertions.assertEquals(1, productRepository.findAll().size());
    }

    private ProductRequest getProductRequest() {
        return ProductRequest.builder()
                .name("banichka")
                .description("ot nikai")
                .price(BigDecimal.valueOf(69))
                .build();
    }

    @Test
    void getShouldGetProduct() throws Exception {
        ProductRequest request = getProductRequest();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/products"))
                .andReturn();

        //TODO - This doesn't work due to random ID generation - fix by adding Json Parsers
        String response = result.getResponse().getContentAsString();
        String expectedResponse = objectMapper.writeValueAsString(generateExpectedResponse());

        Assertions.assertEquals(expectedResponse,response);
    }

    private ProductResponseList generateExpectedResponse() {
        Product product = productMapper.requestToProduct(getProductRequest());
        ProductResponse response = productMapper.productToResponse(product);

        return ProductResponseList.builder()
                .responseList(Collections.singletonList(response))
                .build();
    }


}
